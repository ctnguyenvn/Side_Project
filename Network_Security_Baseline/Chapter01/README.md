### Chapter 1 - Introduction

> Tài liệu: Transllate Network Security Baseline Book

> Thực hiện: Nguyễn Công Trứ

> Cập nhật: 10/05/2017

### Mục lục

- [Security Baseline Overview](#1)

	+  [Preliminary Network Design Asessment](#1.1)

- [Cisco Security Framework Overview](#3)

***

### Giới thiệu

Một mạng an toàn đòi hỏi phải được tích hợp bảo vệ, phòng thủ theo chiều sâu. Lớp đầu tiên của sự tích hợp phòng thủ này là sự thực hiện các yếu tố cơ bản của an ninh mạng. Các yếu tố bảo mật cơ bản này tạo thành cơ sở bảo mật. Tạo ra 1 nền tảng vững chắc đề có thể xây dựng các phương pháp và kỹ thuật tiên tiến hơn.

Việc phát triển và triển khai cơ sở bảo mật là có thể, tuy nhiên gặp nhiều thử thách do có nhiều chức năng có sẵn. Một cơ sở an ninh mạng được thiết kế để hỗ trợ cho nỗ lực này bằng cách phác thảo những yếu tố bảo mật then chốt cần được giải quyết trong giai đoạn đầu của việc triển khai phòng thủ theo chiều sâu. Trọng tâm chính của nền tảng an ninh mạng là đảm bảo cơ sở hạ tầng mạng của chính nó: trình điều khiển và quản lí.

Tài liệu này đưa ra các yếu tố chính được xác định cho cơ sở an ninh mạng, cùng với các hướng dẫn thực hiện để hỗ trợ việc thiết kế, tích hợp và phát triển trong xây dựng mạng.

<a name="1"></a>
### Sơ lược về cơ sở bảo mật

Một nền tảng bảo mật cho thấy các yếu tố an ninh mạng cơ bản là chìa khóa để phát triển 1 nền tảng an ninh mạng mạnh mẽ. Trọng tâm là đảm bảo hạ tầng mạng, như các dịch vụ mạng và giải quyết các vấn đề then chốt về an ninh mạng cơ bản: 

- Truy cập thiết bị cơ sở hạ tầng.

- Định tuyến cơ sở hạ tầng.

- khả năng phục hồi thiết bị.

- Tính toán mạng từ xa.

- Thực thi chính sách mạng.

- Chuyển đổi cơ sở hạ tầng.

Ngoài các yếu tố bảo mật cơ sở được giải quyết, các công nghệ và tính năng bảo mật bổ sung thường vô ích. Ví dụ, nếu tài khoản và mật khẩu mặc định đang hoạt động trên thiết bị cơ sở hạ tầng mạng, nó không cần phải được thực hiện bởi 1 cuộc tấn công tinh vi bởi hacker có thể đăng nhập vào thiết bị 1 cách đơn giản và thực hiện bất kỳ hành động nào mà họ muốn.

Để đảm bảo một giải pháp toàn diện, Cisco Security Framework (CSF) được áp dụng trong việc phát triển 1 nền tảng an ninh mạng. CSF cung cấp một phương pháp toàn diện để đánh giá và xác nhận các yêu cầu an toàn của một hệ thống.

CSF đã được sử dụng trong việc tạo ra nền tảng bảo mật để đảm bảo rằng tất cả các yêu cầu được xem xét cho mỗi khu vực. Tổng quan về phương pháp CSF sẽ được trình sau.

Tất cả cấu hình ở đây đều dựa trên nền tảng và tính năng của cisco IOS. Tuy nhiên, đối tượng bảo mật chung đưa ra trong mỗi phần vẫn có thể áp dụng được trên các nền tảng khác.

<a name="1.1"></a>
### Đánh giá thiết kế mạng sơ bộ

Cơ sở an ninh mạng bao gồm 1 số kỹ thuật bảo mật dựa vào việc thực thi việc lọc lưu lượng dựa trên địa chỉ IP. Chúng bao gồm ACLs để thực thi các chính sách về quyền truy cập quản lí thiết bị, khả năng điều khiển các tuyến đường uRPF. Các kỹ thuật bảo mật tiên tiến hơn có thể được bổ sung như là 1 lớp bảo mật bổ sung, cũng dựa vào lọc lưu lượng trên IP, chẵn hạn như đưa ra các quy tắc tường lửa.

Một lược đồ IP hợp lý, tóm tắt hoặc chia nhỏ, cũng như việc áp dụng các hướng dẫn trong RFC1918, làm cho việc thực hiện các kỹ thuật lọc lưu lượng dựa trên IP này đơn giản và dễ quản lý hơn trên cơ sở liên tục.

Trong chuẩn bị cho việc triển khai 1 đường cơ sở an ninh mạng, nó đánh giá thiết kế mạng sơ bộ được thực hiện để tạo điều kiện cho việc thực hiện. Trọng tâm chính của đánh giá này là xem xét sơ đồ địa chỉ IP hiện tại dưới hai câu hỏi sau:

**Q**. Có phải sơ đồ IP có cấu trúc tốt và nó có thể dể dàng được tóm tắt hoặc phân chia không gian địa chỉ IP không ?

**Q**. Các địa chỉ IP RFC1819 được đẩy vào nơi thích hợp chưa ?

Việc đánh giá sơ đồ địa chỉ IP hiện tại có thể xác định được khu vực yêu cầu lại IP để hoàn tất cơ sở an ninh mạng. Trong khi điều này có thể yêu câu 1 số thay đổi mạng nhưng nó sẽ dẫn đến 1 chính sách an ninh dễ quản lý và thực hiện hơn, cung cấp 1 lợi ích quan trọng đối với an ninh mạng.

Để biết nhiều thông tin hơn ta xem tại RFC1918, see: http://www.ietf.org/rfc/rfc1918.txt .

<a name="2"></a>
### Tổng quan về cơ cấu bảo mật của cisco

The Cisco Security Framework (CSF) là mô hình hoạt động an toàn nhằm đảm bảo mạng lưới, và dịch vụ, tính sẵn sàng và liên tục trong hoạt động kinh doanh. Các mỗi đe dọa luôn thay đổi và CSF được thiết kế để xác định các mối đe dọa hiện tại cũng như theo giỏi các mối đe dọa mới và các mối de dọa đang phát triển, thông qua các phương pháp phổ biến nhất và giải pháp toàn diện nhất.

CSF được xây dựng trên 2 mục tiêu cơ bản, dưới giả thiết rằng người ta không thể kiểm soát được những gì mình không thể thấy hoặc đo được:

- Tăng tầm nhìn tổng quan.

- Xác định, giám xác và tương tác với các sự kiện tren toàn hệ thống.

- Đảm bảo kiểm soát toàn vẹn.

- Củng cố cơ sở hạ tầng, cô lập các host và dịch vụ và thực thi các chính sách bảo mật.

Để đạt được điều này và kiểm soát chúng, nhiều công nghệ đã được sử dụng trong toàn mạng để có thể quan sát được hoạt động của mạng, thực thi các chính sách mạng và xử lý các lưu lượng bất thường. Các yếu tố cơ sở hạ tầng như router và switch được sử dụng làm công cụ giám sát và thực thi các chính sách 1 cách chủ động.

CSF tập trung vào 6 hành động chính như hình sau:

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/Network_Security_Baseline/Image/1-1.png"/></p>

Việc áp dụng CSF vào mạng dẫn đến cần việc xác định các công nghệ và phương pháp phổ biến tốt nhất để đáp ứng cho mỗi hành động trong 6 hành động trên. Tuy nhiên CSF là 1 quá trình liên tục, bao gồm xem xét và chỉnh sửa việc thực hiện phù hợp với sự thay đổi trong kinh doanh và an ninh họ cần.

Để đạt được mục tiêu đó, CSF kết hợp theo chu kỳ phát triển như hình dưới:

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/Network_Security_Baseline/Image/1-2.png"> </p>

Chu trình bắt đầu với sự đánh giá ban đầu nhằm xác định khả năng hiện tại và tình hình bảo mật. Sau đó phân tích các khe hở (như lỗ hổng) để công bố điểm mạnh và điểm yếu của kiến trúc hiện tại.

Cơ sở an ninh mạng được trình bày có thể được sử dụng làm mô hình tham chiếu trong giai đoạn ban đầu đánh giá và phân tích các khe hở (lỗ hổng). Nó cung cấp yêu cầu tối thiểu cho việc kiểm soát và quản lý bảo vệ. Điểm mạnh và điểm yếu của mạng thực (real-world) có thể được xác định bằng việc so sánh chúng với mạng cơ sở.

Sau khi đánh giá ban đầu và phân tích các khe hở, chu kỳ này tiếp tục với kế hoạch khắc phục, nhằm mục đích giảm các khe hở (lỗ hổng) và đáp ứng các yêu cầu trong tương lai bằng cách cập nhật lại toàn bộ kiến trúc mạng.

Plan sequencing (kế hoạch tuần tự) theo sau để thiết lập 1 lộ trình hoàn hảo cho các thành phần khác nhau của kiến trúc dự kiến. Mỗi giai đoạn sau đó được thực hiện và kết quả được đánh giá khi chu kỳ đến assessment phase (giai đoạn đánh giá).

Như hình trên, quá trình này lặp đi lặp lại và mỗi kết quả lặp trong việc phát triển 1 kiến trúc sẽ được thiết kế tốt hơn để đáp ứng được nhu cầu và chính sách bảo mật mà doanh nghiệp cần.

Cơ sở an ninh mạng được phát triển theo CSF. Mỗi phần bao gồm 1 bảng mô tả các tính năng bảo mật đề xuất và phương pháp phổ biến tốt nhất giúp đáp ứng từng hoạt động của CSF.

***
### Tham khảo

[1]. Network Security Baseline. http://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Security/Baseline_Security/securebasebook/sec_chap1.html
***